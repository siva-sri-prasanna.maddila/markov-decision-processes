# Conservation Game

This is the environment for the conservation game. This is a muli-agent competitive game between two teams of agents, named poachers and rangers. This is currently on v2, and (attempts to) implements the model as described in the formal model description.


## Environment 

The main environment is implemented in [conservation_game.py](conservation_game/env/conservation_game.py), following the PettingZoo specification. Some required classes like `GridEnv` and the `Agent`/`Trap` dataclasses are present in [utils](conservation_game/env/utils.py).

## Running tests

Tests can be run from the root folder using `pytest` as follows:

```bash
$ pytest [test/]
```

It is not required to specify the folder, as pytest automatically finds it. To test only the conservation game, it suffices to mention the specific file for the tests, [cons_env_test.py](test/cons_env_test).

```bash
$ pytest test/cons_env_test.py
```

# Poacher Game

This is currently deprecated since its use to form v1.1 of the Conservation Game. (v2) of Conservation Game is based on the use of the formal description, and therefore, does not use Poacher Game anymore.
