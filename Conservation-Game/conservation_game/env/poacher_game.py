"""
Module to implement the conservation game environment
only for poachers and traps. This does not include 
rangers.
"""
import functools
import numpy as np
from gymnasium.spaces import Box, Dict, Discrete, Tuple
from pettingzoo.utils.env import ParallelEnv
from .utils import GridEnv, Trap

# Agents are strings
AgentID = str


class PoacherGame(ParallelEnv):
    """
    Implements the Conservation Game environment with only
    M poachers. We follow the following conventions.

    - Action space:

            0-4 : NOOP, up, left, down, right
            5-6 : (poacher) place_trap, remove_trap
        Assumption: if a poacher is detected, they are caught.

    - Observation space:

        Each agent receives observations based on the neighbouring cells, and
        an action mask on which actions they can take in their current state.
        The observation space is a tuple: the first space is a dictionary of
        (N+M) agent: relative location pairs. Here, the first N channels are
        reserved for the rangers and the last M channels are for the poachers.

        (v1.2) Each observation is a coordinate tuple:

            np.in8.min : unknown location / not detected
            (x,y)      : x, y are np.int8 used to represent the relative position
                         of an agent with respect to the current agent. For example,
                         (-1,2) represents an agent 1 cell to the left and 2 cells
                         above.

        The second channel indicates the presence of traps according to the
        grid_vis. (v1.1) Visibility of traps is by default the agent's current
        cell, and therefore, it contains a count of how many traps there are
        in this cell.

        Assumption:
        Since this game only models poachers, we also assume that
        a poacher can detect the number of their own traps in
        the current cell with probability 1.


    - Rewards:

        Currently, for each reward r given to a poacher p,
        each ranger receives a negative reward of r / num_rangers.

    Version History
    ---------------

    v1.2. Adds agent visibility as a parameter. (Changes observation space to 2D)
    v1.1. Includes traps in the observation space.
    v1.0. Agents identified by string AgentIDs, passing parallel_api_test.
    v0.0. Initial version, unstable.

    """

    metadata = {
        "name": "poacher_game_v1.2",
        "render": ["ansi"],
        "is_parallelizable": True,
    }

    def __init__(
        self,
        grid_size: int = 20,
        npoachers: int = 2,
        ntraps_per_poacher: int = 3,
        prob_detect_cell: float = 0.2,
        prob_animal_appear: float = 0.2,
        grid_vis: int = 1,
        max_time=1e3,
        random_termination_test: bool = False,
    ):
        # game parameters
        self.npoachers = npoachers
        self.prob_animal_appear = prob_animal_appear
        self.prob_detect_cell = prob_detect_cell
        self.ntraps_per_poacher = ntraps_per_poacher
        self.max_time = max_time
        self.curr_time = 0
        self.random_termination_test = random_termination_test
        self.grid_vis = (
            grid_vis  # num. cells that can be seen in any direction
        )

        # agents-related properties
        self.poachers = [f"poacher_{i}" for i in range(self.npoachers)]
        self.poacher_traps = {
            poacher: [
                Trap(name=f"trap_{i}_{poacher}")
                for i in range(self.ntraps_per_poacher)
            ]
            for poacher in self.poachers
        }
        self.agents = self.poachers
        self.possible_agents = self.agents[:]

        # Arena-related properties
        self.gridenv = GridEnv(grid_size, self.grid_vis, self.agents)

        # other parameters.
        self.action_spaces = {ag: Discrete(7) for ag in self.agents}
        self.observation_spaces = {
            agent: Dict(
                {
                    "observations": Tuple(
                        [
                            Box(
                                low=-self.grid_vis,
                                high=self.grid_vis,
                                shape=(len(self.agents), 2),
                                dtype=np.int8,
                            ),  # coordinates of agents relative to current agent
                            Discrete(
                                self.ntraps_per_poacher
                            ),  # num. traps in current cell
                        ]
                    ),
                    "action_mask": Box(
                        low=0, high=1, shape=(7,), dtype=np.int8
                    ),
                }
            )
            for agent in self.agents
        }
        self.total_rewards = dict.fromkeys(
            self.agents, 0.0
        )  # stores every agent's rewards in the current episode.
        self.killed_agents = []

    def reset(self, seed=None, return_info=False, options=None) -> tuple:
        """
        Resets the environment for the next episode
        """
        self.gridenv.reset()
        self.agents = self.possible_agents[:]
        self.curr_time = 0
        observations = {agent: self.observe(agent) for agent in self.agents}
        infos = dict.fromkeys(self.agents, {self.curr_time})
        if not return_info:
            return observations
        return observations, infos

    def observe(self, agent: AgentID) -> dict:
        """
        Each agent can receive observations from the cells around them. These
        can be None, or a dictionary of agents in that cell, according to the
        probability of detection.
        """
        # get the complete observation first
        obs, action_mask = self.gridenv.get_surroundings(agent)
        num_traps_in_current_cell = 0

        # modify action mask to update possible actions
        # Assumption: Poachers can detect their traps in
        #              the current cell with probability 1.
        action_mask[5] = len(self.poacher_traps[agent]) > 0
        num_traps_in_current_cell = len(
            [
                trap
                for trap in obs
                if isinstance(trap, Trap)
                and obs[trap] == self.gridenv.cur_pos[agent]
                and agent in trap.name
            ]
        )
        action_mask[6] = num_traps_in_current_cell > 0

        # encoding the observations:
        # -INT8.MIN_VALUE --> no known location
        # relative X and Y coordinates are sent to each agent,
        # (0,0) refers to the agent's current cell.
        np_obs = np.full(
            shape=(
                len(
                    self.possible_agents,
                ),
                2,
            ),
            fill_value=np.iinfo(np.int8).min,
            dtype=np.int8,
        )
        probas = np.random.random(len(self.possible_agents))

        # Taking the agent ordering from self.possible_agents
        # since self.agents can mutate.
        for i, agent in enumerate(self.possible_agents):
            if agent not in obs or obs[agent] is None:
                continue
            pos = obs[agent]
            (x, y) = pos
            if probas[i] < self.prob_detect_cell:
                np_obs[i, :] = (x, y)

        # return parsed observations
        return {
            "observations": (np_obs, num_traps_in_current_cell),
            "action_mask": action_mask,
        }

    def step(self, actions) -> tuple:
        """
        Receives a joint action, and sends
        the rewards and observations for the next state.
        """
        # increment time-frame
        self.curr_time += 1

        # creating local objects
        rewards = dict.fromkeys(self.agents, 0.0)
        terminations = dict.fromkeys(self.agents, False)

        # calculate the results of the actions for each agent.
        # Negative reward means that the rangers team gets a positive
        # reward, and a poacher will receive the negative reward.
        poacher_reward = 0.0
        for agent in self.agents:
            action = actions[agent]
            assert (
                "poacher" in agent
            ), "Non-poacher agent not allowed for this game"
            poacher_reward = self._step_poacher(agent, action)
            rewards = self._assign_reward(agent, poacher_reward, rewards)

        # stepping through the active traps as well
        # gridenv.cur_pos is a list of all active
        # objects: AgentIDs or Traps
        for obj in self.gridenv.cur_pos:
            # do not consider agents or full traps.
            if not isinstance(obj, Trap) or obj.value > 0:
                continue
            # if trap is empty, animal is caught with some prob
            if np.random.random() < self.prob_animal_appear * obj.efficiency:
                obj.value = 1

        # update terminations, rewards, truncations
        # and infos for the next step.
        time_status = self.curr_time >= self.max_time
        for agent in self.agents:
            terminations[agent] = time_status or (
                self.random_termination_test and np.random.random() > 0.95
            )
            self.total_rewards[agent] += rewards[agent]
        truncations = {agent: False for agent in self.agents}
        infos = {agent: {} for agent in self.agents}

        # update the list of killed agents this round
        self.killed_agents = [
            agent
            for agent in self.agents
            if terminations[agent] or truncations[agent]
        ]

        # update the set of agents and calculate observations
        observations = {agent: self.observe(agent) for agent in self.agents}

        # update the arena
        self._cleanup()

        # return
        return (
            observations,
            rewards,
            terminations,
            truncations,
            infos,
        )

    def render(self):
        """
        Rendering the grid as text for now.
        """
        print(f"\tGrid size: {self.gridenv.N}")
        print("\tActive agents: ", self.agents)
        print(f"\tTime: {self.curr_time}/{self.max_time}\n")
        print(self.gridenv)

    def state(self) -> dict:
        """
        Grid State is stored in the GridEnv class as
        the `grid` attribute.
        """
        return self.gridenv.grid

    def seed(self):
        """
        Seed to make simulations pseudo-deterministic
        """
        return np.random.randint(10000)

    @functools.lru_cache(maxsize=None)
    def observation_space(self, agent):
        """
        Return an agent's observation space.
        """
        return self.observation_spaces[agent]

    @functools.lru_cache(maxsize=None)
    def action_space(self, agent):
        """
        Return an agent's action space.
        """
        return self.action_spaces[agent]

    def _assign_reward(
        self,
        poacher: AgentID = None,
        reward: float = 0.0,
        rewards: dict = None,
    ) -> None:
        """
        Assigns the reward to poacher, and splits the reward among the
        cooperative rangers. A positive reward adds to poacher and
        removes proportionally from all rangers.
        """
        if rewards is None:
            raise RuntimeError("No reward dict to update!")
        if poacher is None:
            # Randomly choose a poacher to receive
            poacher = next(
                (ag for ag in self.agents if "poacher" in ag),
                None,
            )
        assert poacher is not None, "No poacher found to assign reward"
        rewards[poacher] += reward
        return rewards

    def _step_poacher(self, poacher: AgentID, action: int) -> float:
        """
        Steps through for a poacher, performs their action
        and returns the reward
        """
        assert (
            "poacher" in poacher
        ), "Non-poacher agent passed to _step_poacher"
        reward = 0.0

        # any action will throw if unauthorised by action_mask
        if 1 <= action <= 4:
            # Movement;
            self.gridenv.update_position(poacher, action)
        elif action == 5 and len(self.poacher_traps[poacher]) > 0:
            # Place trap
            trap = self.poacher_traps[poacher].pop()
            self.gridenv.add_trap(trap, self.gridenv.cur_pos[poacher])
        elif action == 6:
            # Remove trap
            reward, trap = self.gridenv.remove_trap(
                self.gridenv.cur_pos[poacher]
            )
            self.poacher_traps[poacher].append(trap)
        return reward

    def _cleanup(self) -> None:
        """
        Update the set of live agents, and their position
        on the grid.
        """
        for agent in self.killed_agents:
            # clean GridEnv lists
            _pos = self.gridenv.cur_pos[agent]
            self.gridenv.grid[_pos].remove(agent)
            del self.gridenv.cur_pos[agent]

            # clean agent lists
            self.agents.remove(agent)
