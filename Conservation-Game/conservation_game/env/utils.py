""" Utils for the environment ConservationGame. 
Defines the GridEnv and the agent dataclasses.
"""
from copy import deepcopy
from dataclasses import dataclass
import numpy as np


@dataclass(eq=True)
class Trap:
    """
    Dataclass to support the traps a poacher
    can place, as well as the environment
    can detect. Its value is non-zero if
    an animal has been caught in it.
    """

    name: str = "Trap"
    efficiency: float = 0.40
    value: float = 0.0

    def __hash__(self):
        """
        Overriding hash to be hash of Trap.name
        """
        return hash(self.name)

    def __repr__(self):
        """
        Overriding the representation for
        better printing
        """
        return self.name


class GridState:
    """
    Class to represent the game state for the Conservation Game.
    State is represented as a dictionary of player-(location-misc)
    pairs.

    v2. Deprecates the grid dictionary for simpler code.
        The GridEnv class is now upgraded to the GridState class,
        and stores the complete state, as described in the
        model document.

    v1. First stable version without bugs, but GridEnv maintained both
        a grid and a current positions list.
    """

    REWARD_MAP: dict = {"TRAP_FOUND": 2, "POACHER_FOUND": 8}
    NULL_POS: np.array = np.array([-1, -1, 0, 0])

    def __init__(
        self,
        grid_size: int,
        rangers: list[str],
        poachers: list[str],
        ntraps_per_poacher: int,
    ):
        self.N = grid_size
        self.state = {
            **{
                ranger: np.random.randint(self.N, size=2) for ranger in rangers
            },
            **{
                poacher: np.concatenate(
                    (
                        np.random.randint(self.N, size=2),  # random location
                        np.array(
                            [ntraps_per_poacher, 0]
                        ),  # (..., ntraps, npreys)
                    )
                )
                for poacher in poachers
            },
        }
        self.init_state = deepcopy(self.state)

    def get_neighbours(self, agent: str) -> tuple:
        """
        Given an agent, fetch his neighbours.
        """
        agent_pos = self.state[agent][0:2]
        return [
            _a
            for _a in self.state
            if (not isinstance(_a, Trap))
            and all(self.state[_a][0:2] == agent_pos)
        ]

    def permitted_movements(self, agent: str) -> np.array:
        """
        For an agent, return the permitted actions she can
        take in her current cell. Returns an numpy.array that
        represents an action mask.
        """
        if "ranger" in agent:
            size_action_mask = 5
        elif "poacher" in agent:
            size_action_mask = 6
        action_mask = np.zeros(size_action_mask, dtype=np.int8)

        # Find the agent position
        x, y = self.state[agent][0:2]
        if x == -1 or y == -1:
            return np.zeros(6, dtype=np.int8)

        # calculate permitted actions
        UP = 0 if x <= 0 else 1
        DOWN = 0 if x >= self.N - 1 else 1
        LEFT = 0 if y <= 0 else 1
        RIGHT = 0 if y >= self.N - 1 else 1

        action_mask[:5] = [1, UP, LEFT, DOWN, RIGHT]
        return action_mask

    def update_position(self, agent: str, action: int) -> None:
        """
        Updates the position of an agent in the grid,
        and updates the internal current positions list.
        """
        # calculate the new position
        new_pos = None
        x, y = self.state[agent][0:2]
        if action == 1:  # UP
            new_pos = (x - 1, y)
        elif action == 2:  # LEFT
            new_pos = (x, y - 1)
        elif action == 3:  # DOWN
            new_pos = (x + 1, y)
        elif action == 4:  # RIGHT
            new_pos = (x, y + 1)

        # Throw error if invalid position generated
        # since it needed an invalid action
        assert self.valid_pos(
            new_pos
        ), f"update_position({agent}): Invalid action. old:{(x,y)} --> action:{action} -> new:{new_pos}"

        # update grid and state list
        self.state[agent][0:2] = new_pos

    def add_trap(self, trap: Trap, pos: tuple) -> None:
        """
        Adds a trap to the supplied position.
        """
        self.state[trap] = pos

    def remove_poacher(self, poacher: str) -> float:
        """
        Tries to remove an agent from the supplied position.
        Returns a positive reward if successful, else throws an exception.
        """
        assert poacher in self.state, f"Unauthorised remove !!!\n{self}"
        self.state[poacher] = GridState.NULL_POS
        return GridState.REWARD_MAP["POACHER_FOUND"]

    def remove_trap(self, trap: Trap) -> float:
        """
        Tries to remove a Trap from the supplied position.
        Returns a positive reward if successful, else throws an exception.
        """
        assert trap in self.state, f"Unauthorised remove !!!\n{self}"
        del self.state[trap]
        return GridState.REWARD_MAP["TRAP_FOUND"]

    def valid_pos(self, pos: tuple) -> bool:
        """
        Checks if a position is valid for the given grid.
        """
        return (0 <= pos[0] < self.N) and (0 <= pos[1] < self.N)

    def reset(self) -> None:
        """
        Resets the grid to initial position
        """
        self.state = deepcopy(self.init_state)

    def __str__(self) -> str:
        """Gets printable version of GridEnv objects"""
        return "\n".join(
            [f"{thing}: {self.state[thing]}" for thing in self.state]
        )
