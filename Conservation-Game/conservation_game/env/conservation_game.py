"""
Module to implement the conservation game environment
"""

import functools
import numpy as np
from gymnasium.spaces import Dict, Discrete, MultiDiscrete, MultiBinary
from pettingzoo.utils.env import ParallelEnv
from .utils import GridState, Trap

# Agents are strings
AgentID = str


class ConservationGame(ParallelEnv):
    """
    Implements the Conservation Game environment with
    M poachers and N rangers. We follow the following conventions.

    - Action space:

            0-4 : NOOP, up, left, down, right
            5 : (poacher) place_trap

    - Observation space:

        Each observation space is a composite product of smaller spaces.
        This reflects the formal definition of the model.

        Assumption:
        We also assume that a poacher can detect the number of their own traps in
        the current cell with probability 1.


    - Rewards:

        Currently, for each reward r given to a poacher p,
        each ranger receives a negative reward of r / num_rangers.

    Version History
    ---------------

    v2.0. Re-implements env to use the developed model specification.
    v1.0. Builds from implementation 1.2 of the PoacherGame.
    v0.0. Initial version, unstable.
    """

    metadata = {
        "name": "conservation_game_v2.0",
        "render": ["ansi"],
        "is_parallelizable": True,
    }

    def __init__(
        self,
        grid_size: int = 20,
        npoachers: int = 2,
        nrangers: int = 2,
        ntraps_per_poacher: int = 3,
        prob_detect_cell: float = 0.2,
        prob_animal_appear: float = 0.2,
        prob_detect_trap: float = 0.1,
        max_time=1e3,
    ):
        # time properties
        self.max_time = max_time
        self.curr_time = 0

        # agent parameters
        self.ntraps_per_poacher = ntraps_per_poacher
        self.prob_animal_appear = prob_animal_appear
        self.prob_detect_cell = prob_detect_cell
        self.prob_detect_trap = prob_detect_trap

        # agents-related properties
        self.poachers = [f"poacher_{i}" for i in range(npoachers)]
        self.poacher_traps = {
            poacher: [
                Trap(name=f"trap_{i}_{poacher}")
                for i in range(self.ntraps_per_poacher)
            ]
            for poacher in self.poachers
        }
        self.rangers = [f"ranger_{i}" for i in range(nrangers)]
        self.agents = self.rangers + self.poachers
        self.possible_agents = self.agents[:]

        # Arena-related properties
        self.grid = GridState(
            grid_size, self.rangers, self.poachers, self.ntraps_per_poacher
        )

        # Spaces parameters.
        self.action_spaces = {
            **{ranger: Discrete(5) for ranger in self.rangers},
            **{poacher: Discrete(6) for poacher in self.poachers},
        }
        self.observation_spaces = {
            **{
                poacher: Dict(
                    {
                        "state": MultiDiscrete(
                            [
                                grid_size,
                                grid_size,
                                ntraps_per_poacher,
                                np.iinfo(np.int32).max,
                            ]
                        ),  # poacher state is (x,y,ntraps,npreys)
                        "ground_traps": MultiDiscrete(
                            [ntraps_per_poacher, ntraps_per_poacher]
                        ),  # traps found in current cell
                        "rangers": Discrete(
                            nrangers
                        ),  # num. of rangers detected in current cell
                        "poachers": Discrete(
                            npoachers
                        ),  # num. of poachers detected in current cell
                    }
                )
                for poacher in self.poachers
            },
            **{
                ranger: Dict(
                    {
                        "state": MultiDiscrete(
                            [grid_size, grid_size]
                        ),  # ranger state is location
                        "partners": MultiBinary(
                            nrangers
                        ),  # for the list of rangers sharing the cell
                        "poacher_traps": Dict(
                            {
                                poacher: MultiDiscrete(
                                    [ntraps_per_poacher, ntraps_per_poacher]
                                )
                                for poacher in self.poachers
                            }
                        ),  # For the traps and prey recovered from poacher capture
                        "ground_traps": MultiDiscrete(
                            [ntraps_per_poacher, ntraps_per_poacher]
                        ),  # For traps recovered from the grid
                    }
                )
                for ranger in self.rangers
            },
        }
        self.total_rewards = dict.fromkeys(
            self.agents, 0.0
        )  # stores every agent's rewards in the current episode.
        self.killed_agents = []

    def reset(self, seed=None, return_info=False, options=None) -> tuple:
        """
        Resets the environment for the next episode
        """
        self.grid.reset()
        self.agents = self.possible_agents[:]
        self.poacher_traps = {
            poacher: [
                Trap(name=f"trap_{i}_{poacher}")
                for i in range(self.ntraps_per_poacher)
            ]
            for poacher in self.poachers
        }
        self.curr_time = 0
        observations = {
            agent: {
                "observations": {},
                "action_mask": self.grid.permitted_movements(agent),
            }
            for agent in self.agents
        }
        if not return_info:
            return observations
        # else, generate information
        infos = dict.fromkeys(self.agents, {self.curr_time})
        return observations, infos

    def observe(self, agent: AgentID, record: dict) -> dict:
        """
        Each agent receives observations from their current cell. These
        are calculated deterministically based on their record, which is
        a record of all relevant information during the transition to
        their new state.
        """
        assert (
            "poacher" in agent or "ranger" in agent
        ), f"Unknown agent passed as argument! {agent}"

        # get the complete observation first
        print(f"Getting observations for {agent}")
        action_mask = self.grid.permitted_movements(agent)

        if "poacher" in agent:
            action_mask[5] = (
                len(self.poacher_traps[agent]) > 0
            )  # authorise place_trap
            obs = {
                "state": self.grid.state[agent],
                "ground_traps": np.array(
                    [record["trap_empty"], record["trap_full"]]
                ),
                "rangers": record["num_rangers"],
                "poachers": record["num_poachers"],
            }
        elif "ranger" in agent:
            partners = np.zeros(shape=(len(self.rangers),), dtype=np.int8)
            partners[record["partners"]] = 1

            obs = {
                "state": self.grid.state[agent],
                "partners": partners,
                "poacher_traps": {
                    poacher: np.array([nempty, nfull])
                    for poacher, (nempty, nfull) in record[
                        "poachers_found"
                    ].items()
                },
                "ground_traps": np.array(
                    [record["trap_empty"], record["trap_full"]]
                ),
            }

        # return parsed observations
        print(f"\t{agent} has actions authorised: {action_mask}")
        return {
            "observations": obs,
            "action_mask": action_mask,
        }

    def step(self, actions: dict) -> tuple:
        """
        Receives a joint action, and sends
        the rewards and observations for the next state.
        """
        self.curr_time += 1

        # creating local objects ...
        # records tracks transition information that
        # is used to create the observations of s^{t+1}.
        rewards = dict.fromkeys(self.agents, 0.0)
        terminations = dict.fromkeys(self.agents, False)
        records = dict.fromkeys(self.agents)
        for agent in self.agents:
            if "ranger" in agent:
                records[agent] = {
                    "trap_empty": 0,
                    "trap_full": 0,
                    "poachers_found": {
                        poacher: [0, 0]
                        for poacher in self.poachers  # nempty, nfull
                    },
                    "partners": [],
                }
            elif "poacher" in agent:
                records[agent] = dict.fromkeys(
                    ["trap_empty", "trap_full", "num_rangers", "num_poachers"],
                    0,
                )

        # ... and now we run through the transitions !
        # Step 1: Rangers move first
        self._rangers_move(actions, records)

        # Step 2: Poachers move and remove their traps
        self._poachers_move_and_get_traps(actions, rewards, records)

        # Step 3: Rangers remove traps and remaining traps capture animals
        self._rangers_remove_traps(rewards, records)
        self._traps_catch_animals()

        # Step 4: Rangers remove poachers and remaining poachers place traps
        self._rangers_remove_poachers(rewards, terminations, records)
        self._poachers_place_traps(actions, terminations)

        # update terminations: dict, rewards: dict, truncations and infos for the next step.
        time_status = self.curr_time >= self.max_time
        all_poachers_caught = all(
            (
                terminations[agent]
                for agent in self.poachers
                if agent in self.agents
            )
        )
        for agent in self.agents:
            terminations[agent] |= time_status or all_poachers_caught
            self.total_rewards[agent] += rewards[agent]
        truncations = {agent: False for agent in self.agents}

        # update the list of killed agents this round
        self.killed_agents = [
            agent
            for agent in self.agents
            if terminations[agent] or truncations[agent]
        ]

        # calculate info for next step.
        infos = {agent: {} for agent in self.agents}
        observations = {
            agent: self.observe(agent, records[agent]) for agent in self.agents
        }

        # update the arena
        self._cleanup()

        return (
            observations,
            rewards,
            terminations,
            truncations,
            infos,
        )

    def render(self):
        """
        Rendering the grid as text for now.
        """
        print(f"\tGrid size: {self.grid.N}")
        print("\tActive agents: ", self.agents)
        print(f"\tTime: {self.curr_time}/{self.max_time}\n")
        print(self.grid)

    def state(self) -> dict:
        """
        Grid State is stored in the GridState class as
        the `grid` attribute.
        """
        return self.grid.state

    def _assign_reward(
        self, poacher: AgentID, reward: float, rewards: dict
    ) -> None:
        """
        Assigns the reward to poacher, and splits the reward among the
        cooperative rangers. A positive reward adds to poacher and
        removes proportionally from all rangers.
        """
        assert rewards is not None, "No reward dict to update!"

        # If assigning a 'reward' to an active player.
        # This arises because rangers can recover a trap of a
        # captured agent. Since he is captured, he is not
        # in rewards (which is initialised from self.agents,
        # the list of active agents).
        if poacher in rewards:
            rewards[poacher] += reward
        else:
            print(f"Assigning {poacher} a total reward of {reward} !!!")
            self.total_rewards[poacher] += reward

        for ranger in self.rangers:
            rewards[ranger] -= reward / len(self.rangers)

    def _rangers_move(self, actions: dict, records: dict) -> None:
        """
        Helper function to move the rangers and update their records
        with detected partners in the same cell.
        """
        for ranger in [r for r in self.rangers if 1 <= actions[r] <= 4]:
            self.grid.update_position(ranger, actions[ranger])
            for nbor_ranger in [
                r
                for r in self.rangers
                if r != ranger
                and all(self.grid.state[r] == self.grid.state[ranger])
            ]:
                # add the ranger number to the records
                records[ranger]["partners"].append(
                    int(nbor_ranger.split("_")[-1])
                )

    def _poachers_move_and_get_traps(
        self, actions: dict, rewards: dict, records: dict
    ) -> None:
        """
        Helper function: Moves the poachers according to actions
        and removes their traps (if found) on the next step.
        """
        for poacher in [
            p
            for p in self.poachers
            if p in self.agents and 1 <= actions[p] <= 4
        ]:
            self.grid.update_position(poacher, actions[poacher])
            for _trap in [
                _t
                for _t in self.grid.state
                if isinstance(_t, Trap)
                and all(self.grid.state[_t] == self.grid.state[poacher][0:2])
                and poacher in _t.name
            ]:
                self.poacher_traps[poacher].append(_trap)
                self._assign_reward(
                    poacher, self.grid.remove_trap(_trap), rewards
                )

                # update trap records
                key = "trap_empty" if _trap.value == 0.0 else "trap_full"
                records[poacher][key] += 1

            # Update records for positions as well
            for nbor in self.grid.get_neighbours(poacher):
                if "poacher" in nbor:
                    records[poacher]["num_poachers"] += 1
                else:
                    records[poacher]["num_rangers"] += 1

    def _rangers_remove_traps(self, rewards: dict, records: dict):
        """
        Helper function where rangers detect and
        remove traps in their current cells.
        Note that detection depends on self.prob_detect_trap.
        """
        for ranger in self.rangers:
            for _trap in [
                _t
                for _t in self.grid.state
                if isinstance(_t, Trap)
                and all(self.grid.state[_t] == self.grid.state[ranger])
                and np.random.random() < self.prob_detect_trap
            ]:
                # Punish the owning poacher, even when retired.
                _poacher = "_".join(_trap.name.split("_")[-2:])

                # updating the records
                key = "trap_empty" if _trap.value == 0.0 else "trap_full"
                records[ranger][key] += 1
                self._assign_reward(
                    _poacher, -self.grid.remove_trap(_trap), rewards
                )

    def _rangers_remove_poachers(
        self, rewards: dict, terminations: dict, records: dict
    ):
        """
        Helper function where rangers detect and
        remove poachers in their current cell.
        Note that detection depends on self.prob_detect_cell
        """
        for ranger in self.rangers:
            for _poacher in [
                _p
                for _p in self.grid.state
                if isinstance(_p, str)
                and all(self.grid.state[_p][0:2] == self.grid.state[ranger])
                and "poacher" in _p
                and np.random.random() < self.prob_detect_cell
            ]:
                # negative reward for the caught poacher.
                terminations[_poacher] = True
                self._assign_reward(
                    _poacher, -self.grid.remove_poacher(_poacher), rewards
                )

                # updating records
                for _trap in self.poacher_traps[_poacher]:
                    key = 0 if _trap.value == 0.0 else 1
                    records[ranger]["poachers_found"][_poacher][key] += 1

    def _poachers_place_traps(self, actions: dict, terminations: dict):
        """
        Helper function where poachers place traps.
        Note that this will not succeed if poacher has
        no traps to place.
        """
        for poacher in [
            p
            for p in self.poachers
            if (p in self.agents) and (not terminations[p]) and actions[p] == 5
        ]:
            trap = self.poacher_traps[poacher].pop()
            self.grid.add_trap(trap, self.grid.state[poacher][0:2])

    def _traps_catch_animals(self):
        """
        Helper function where currently active
        traps catch prey if they have not already.
        """
        for trap in [t for t in self.grid.state if isinstance(t, Trap)]:
            if (
                np.random.random() < self.prob_animal_appear
                and trap.value == 0.0
            ):
                trap.value += 1

    def _cleanup(self) -> None:
        """
        Update the set of live agents, and their position
        on the grid.
        """
        for agent in self.killed_agents:
            # clean GridState lists
            del self.grid.state[agent]
            self.agents.remove(agent)

    def seed(self):
        """
        Seed to make simulations pseudo-deterministic
        """
        return np.random.randint(10000)

    @functools.lru_cache(maxsize=None)
    def observation_space(self, agent):
        """
        Return an agent's observation space.
        """
        return self.observation_spaces[agent]

    @functools.lru_cache(maxsize=None)
    def action_space(self, agent):
        """
        Return an agent's action space.
        """
        return self.action_spaces[agent]
