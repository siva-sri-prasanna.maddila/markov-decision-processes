"""
Personal testing module for the conservation_game
environment and related modules. 
Usage: (in Conservation-Game/)
    $ pytest test/
"""

from copy import deepcopy
import pytest
from gymnasium.spaces import Discrete
from pettingzoo.test import (
    parallel_api_test,
    max_cycles_test,
    performance_benchmark,
)
from pettingzoo.test.seed_test import parallel_seed_test
from conservation_game.conservation_game_v0 import *


@pytest.fixture()
def lcons_game():
    """
    Creates a particular instance of the conservation game class
    to be used in all the tests."""
    print("Creating new fixture instance of ConsGame ... ")
    yield ConservationGame(
        grid_size=5,
        npoachers=5,
        nrangers=4,
        ntraps_per_poacher=10,
        prob_detect_cell=0.5,
        prob_animal_appear=0.5,
        prob_detect_trap=0.5,
        max_time=500,
    )
    print("End of fixture ...")


@pytest.fixture()
def cons_game():
    """
    Creates a particular instance of the conservation game class
    to be used in all the tests.
    """
    print("Creating new (short) fixture instance of ConsGame ... ")
    yield ConservationGame(grid_size=3)
    print("End of (short) fixture ...")


def test_module():
    """
    Checks the metadata to ensure the right environment is used.
    """
    assert "conservation_game_v2.0" == ConservationGame.metadata["name"]


def test_parallel_test_api_suite(lcons_game):
    """
    Runs the parallel_api_test for multiple grid sizes.
    """
    for grid_size in range(1, 3 + 1):
        parallel_api_test(
            ConservationGame(grid_size=grid_size), num_cycles=1000
        )
    parallel_api_test(lcons_game, num_cycles=1000)


@pytest.mark.skip(
    reason="Test fails if we use np.random directly, which we do"
)
def test_parallel_seed_test():
    """
    Runs the seed test on the Conservation game.
    """
    parallel_seed_test(ConservationGame, num_cycles=10, test_kept_state=True)


@pytest.mark.xfail(
    reason="Game has no attribute parallel_env. To write wrappers!"
)
def test_max_cycles_test():
    """
    Runs the state test on the Conservation game.
    """
    max_cycles_test(ConservationGame)


@pytest.mark.skip(reason="This is for the AEC model, requires agent_iter")
def test_performance_parenv(lcons_game):
    """
    Runs the state test on the Conservation game.
    """
    performance_benchmark(lcons_game)


def test_gridstate():
    """
    Tests get_surroundings. Uses a grid of size 3 so that
    some actions will definitely be invalid. Otherwise, tests
    that all behaviour is as intended.
    """
    poachers = [f"poacher_{i}" for i in range(5)]
    poacher_traps = {
        poacher: [Trap(f"trap_{i}_{poacher}") for i in range(3)]
        for poacher in poachers
    }
    rangers = [f"ranger_{i}" for i in range(5)]
    g = GridState(
        grid_size=3, rangers=rangers, poachers=poachers, ntraps_per_poacher=3
    )
    for agent in poachers + rangers:
        pos = g.state[agent]
        print(agent, g.valid_pos(pos))
        assert g.valid_pos(
            pos
        ), f"Default init must be valid: {agent} @ pos:{pos} (grid_size:{g.N})"

    for poacher in poachers:
        trap = poacher_traps[poacher].pop()
        g.add_trap(trap, g.state[poacher][0:2])

    ctr = 0
    for thing in g.state:
        assert isinstance(thing, Trap) or isinstance(
            thing, str
        ), "Only agents or traps allowed!"
        ctr = ctr + 1 if isinstance(thing, Trap) else ctr

    assert ctr == len(
        poachers
    ), "Each poacher should have placed a trap! ctr:{ctr}"

    for agent in rangers + poachers:
        x, y = g.state[agent][0:2]
        valid_actions = g.permitted_movements(agent)

        # Generating all possible steps
        up_pos = (x - 1, y)
        left_pos = (x, y - 1)
        down_pos = (x + 1, y)
        right_pos = (x, y + 1)

        # asserting they are as intended
        assert (
            g.valid_pos(up_pos) == valid_actions[1]
        ), "Invalid action allowed!"
        assert (
            g.valid_pos(left_pos) == valid_actions[2]
        ), "Invalid action allowed!"
        assert (
            g.valid_pos(down_pos) == valid_actions[3]
        ), "Invalid action allowed!"
        assert (
            g.valid_pos(right_pos) == valid_actions[4]
        ), "Invalid action allowed!"

    for obj in list(g.state.keys()).copy():
        if isinstance(obj, Trap):
            g.remove_trap(obj)
        else:
            g.remove_poacher(obj)

    for thing in g.state:
        assert isinstance(thing, str), "No traps should have remained!"

    for poacher in poachers:
        assert all(
            g.state[poacher] == GridState.NULL_POS
        ), "All poachers should have been retired!"


def test_environment(cons_game, every: int = 20):
    """
    Simulates the environment until done.
    """
    done = False
    observations, terminations, truncations = (
        None,
        None,
        None,
    )
    action_mask = {
        agent: cons_game.grid.permitted_movements(agent)
        for agent in cons_game.agents
    }
    print("-" * 80)
    while not done:
        # sample the actions for each agent
        actions = {
            agent: cons_game.action_space(agent).sample(
                mask=action_mask[agent]
            )
            for agent in cons_game.agents
        }
        # step through the environment
        observations, _, terminations, truncations, _ = cons_game.step(actions)

        # update the possible actions for each agent
        action_mask = {
            agent: observations[agent]["action_mask"]
            for agent in cons_game.agents
        }

        # post-processing
        done = all(
            [
                x or y
                for x, y in zip(terminations.values(), truncations.values())
            ]
        )
        if cons_game.curr_time % every == 0 or done:
            cons_game.render()
            print("-" * 80)

    print(cons_game.curr_time, " is the current time")
    print(cons_game.max_time, " is the maximum time")
    print("Rewards: ")
    for agent in cons_game.possible_agents:
        print(agent, " has ", cons_game.total_rewards[agent])
    assert sum(cons_game.total_rewards.values()) == 0.0


def test_init_cons_game(lcons_game):
    """
    Tests the initialisation of the ConsGame
    """

    assert (
        lcons_game.metadata["name"] == "conservation_game_v2.0"
    ), f"Testing incorrect version (currently {lcons_game.metadata['name']})"

    # Check the lists of created agents
    assert len(lcons_game.poachers) == 5, "Too many or few poachers created."
    assert len(lcons_game.rangers) == 4, "Too many or few rangers created."
    assert set(lcons_game.agents) == set(
        lcons_game.rangers + lcons_game.poachers
    ), "Number of agents does not match the set of agents"
    assert (
        sum((len(traps) for _, traps in lcons_game.poacher_traps.items()))
        == 10 * 5
    ), "Number of traps is wrong!"
    assert all(
        (
            Discrete(5) == lcons_game.action_spaces[ranger]
            for ranger in lcons_game.rangers
        )
    ), "Incorrect Ranger action space created."

    # Check the observation and action spaces.
    assert set(lcons_game.rangers + lcons_game.poachers) == set(
        lcons_game.action_spaces.keys()
    ), "Not all agents have actions, or there are too many agents."
    assert all(
        (
            Discrete(6) == lcons_game.action_spaces[poacher]
            for poacher in lcons_game.poachers
        )
    ), "Incorrect Poacher action space created."

    assert set(lcons_game.rangers + lcons_game.poachers) == set(
        lcons_game.observation_spaces.keys()
    ), "Not all agents have observations, or there are too many agents."
    assert all(
        (
            set(["state", "ground_traps", "rangers", "poachers"])
            == set(lcons_game.observation_spaces[poacher].keys())
            for poacher in lcons_game.poachers
        )
    ), "Unknown poacher observation structure"
    assert all(
        (
            set(["state", "partners", "poacher_traps", "ground_traps"])
            == set(lcons_game.observation_spaces[ranger].keys())
            for ranger in lcons_game.rangers
        )
    ), "Unknown ranger observation structure"

    # Check rewards
    assert set(lcons_game.total_rewards.keys()) == set(
        lcons_game.agents
    ), "Unknown rewards structure"


def test_reset(lcons_game):
    """
    Tests the reset function of the ConservationGame environment.
    """
    lcopy = deepcopy(lcons_game)

    # Simulating a game.
    lcons_game.agents.remove("poacher_1")
    lcons_game.poacher_traps["poacher_1"] = []

    assert set(lcopy.agents) != set(
        lcons_game.agents
    ), "Removed agent does not change agents set."
    assert set(lcopy.poacher_traps["poacher_1"]) != set(
        lcons_game.poacher_traps["poacher_1"]
    ), "Removing traps does not change traps set."
    assert all(
        (
            set(lcopy.poacher_traps[poacher])
            == set(lcons_game.poacher_traps[poacher])
            for poacher in lcons_game.poachers
            if "poacher_1" != poacher
        )
    ), "Other agents should be unaffected by the change."

    # reset should change this behavious
    lcons_game.reset()
    assert set(lcopy.agents) == set(
        lcons_game.agents
    ), "Reset does not reset agents list."
    assert all(
        (
            set(lcopy.poacher_traps[poacher])
            == set(lcons_game.poacher_traps[poacher])
            for poacher in lcopy.poachers
        )
    ), "Reset does not reset poachers' traps."


def test_observe(lcons_game):
    """
    Tests the received observations
    """
    with pytest.raises(AssertionError):
        # Agent does not exist, therefore should throw.
        lcons_game.observe("agent", {"invalid": "data"})
    with pytest.raises(KeyError):
        # Record is empty, therefore should throw.
        lcons_game.observe("poacher_0", {})
    with pytest.raises(KeyError):
        # Record is invalid, therefore should throw.
        lcons_game.observe("poacher_0", {"invalid": "data"})
    with pytest.raises(KeyError):
        # Record is invalid, therefore should throw.
        lcons_game.observe("ranger_0", {"invalid": "data"})


def test_assign_reward(cons_game):
    """
    Tests all the branches of the _assign_reward helper
    function
    """
    for poacher in cons_game.poachers.copy():
        rewards = dict.fromkeys(cons_game.agents, 0)
        cons_game._assign_reward(poacher, 1, rewards)

        assert rewards[poacher] == 1
        assert (
            cons_game.total_rewards[poacher] == 0
        ), "We did not assign anything here!!"
        assert sum((rewards[ranger] for ranger in cons_game.rangers)) == -1

        # Force assigning a reward after the poacher is `caught`
        cons_game.agents.remove(poacher)
        del rewards[poacher]
        cons_game._assign_reward(poacher, 1, rewards)

        assert (
            cons_game.total_rewards[poacher] == 1
        ), "total_rewards not updated for retired agent"
        assert (
            sum((rewards[ranger] for ranger in cons_game.rangers)) == -2
        ), "ranger rewards not updated when assigning reward to retired agent"
