"""
Personal testing module for the conservation_game
environment and related modules. 
Usage: (in Conservation-Game/)
    $ pytest test/
"""

import numpy as np
from tqdm import tqdm
import pytest
from gymnasium.spaces import Discrete
from conservation_game.conservation_game_v0 import *
from pettingzoo.test import parallel_api_test, performance_benchmark


@pytest.mark.skip(reason="PoacherGame is now deprecated.")
def test_parallel_api():
    """
    Runs the parallel_api_test that every
    ParallelEnv must pass on ConsGame
    """
    poacher_game = PoacherGame(grid_size=4, random_termination_test=True)
    parallel_api_test(poacher_game)


@pytest.mark.skip(reason="PoacherGame is now deprecated.")
def test_environment():
    """
    Simulates the environment until done.
    """
    poacher_game = PoacherGame(grid_size=4, random_termination_test=False)
    done = False
    observations, reward, terminations, truncations, infos = (
        None,
        None,
        None,
        None,
        None,
    )
    observations = {
        agent: poacher_game.observe(agent) for agent in poacher_game.agents
    }
    action_mask = {
        agent: observations[agent]["action_mask"]
        for agent in poacher_game.agents
    }
    print("Action mask: ", action_mask)
    print("-" * 80)
    while not done:
        # sample the actions for each agent
        actions = {
            agent: poacher_game.action_space(agent).sample(
                mask=action_mask[agent]
            )
            for agent in poacher_game.agents
        }
        # step through the environment
        (
            observations,
            reward,
            terminations,
            truncations,
            infos,
        ) = poacher_game.step(actions)

        # update the possible actions for each agent
        action_mask = {
            agent: observations[agent]["action_mask"]
            for agent in poacher_game.agents
        }

        # post-processing
        done = all(
            [
                x or y
                for x, y in zip(terminations.values(), truncations.values())
            ]
        )
        poacher_game.render()
        print("-" * 80)

    print("Rewards: ")
    for agent in poacher_game.possible_agents:
        print(agent, " has ", poacher_game.total_rewards[agent])
        assert poacher_game.total_rewards[agent] >= 0
