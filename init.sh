#!/bin/bash
# intialisation script to create the environment and 
# install dependencies as found in requirements.txt
# Author : Prasanna <maddilaprasanna10@gmail.com>
# Created: 03 May 2023
# Last Modified: 20 July, 2023

# Create virtual env
virtualenv .venv; 
source .venv/bin/activate;

# install requirements and test installation
pip install -r requirements.txt
python tests/lunar_lander.py
