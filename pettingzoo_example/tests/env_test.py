from pettingzoo_example import posg_env_v0
from pettingzoo.test import parallel_api_test

parallel_api_test(
    posg_env_v0.PosgEnvironment(posg_env_v0.POSG()), num_cycles=20
)
