import ray
from ray.tune.registry import register_env
from ray.rllib.env.wrappers.pettingzoo_env import ParallelPettingZooEnv
from pettingzoo_example import posg_env_v0
from ray.rllib.algorithms.pg import PGConfig
from ray.rllib.algorithms.ppo import PPOConfig
from ray.tune.logger import pretty_print
from ray import air, tune

# import torch
# import simpy

if __name__ == "__main__":
    ray.init()
    # Import the pettingzoo environment in the way needed by rllib
    env_name = "posgenv_v0"
    register_env(
        env_name,
        lambda config: ParallelPettingZooEnv(
            posg_env_v0.PosgEnvironment(posg_env_v0.POSG())
        ),
    )
    config = (
        PPOConfig()
        .environment("posgenv_v0")
        .rollouts(
            num_rollout_workers=1,
            # Fragment length, collected at once from each worker
            # and for each agent!
            rollout_fragment_length=30,
        )
        # Training batch size -> Fragments are concatenated up to this point.
        .training(train_batch_size=200)
    )

    # config = PGConfig().environment("posgenv_v0")
    algo = config.build()
    result = list()
    for i in range(10):
        result.append(algo.train())

    algo.stop()
    ray.shutdown()
    for i in range(10):
        print(f"Result at iteration {i}:")
        print(pretty_print(result[i]))

    # stop = {
    #   "training_iteration": 2,
    # }
    # results = tune.Tuner("PG", param_space=config, run_config=air.RunConfig(stop=stop, verbose=1)).fit()
