from ray.rllib.policy.policy import Policy
from ray.tune.registry import register_env
from ray.rllib.env.wrappers.pettingzoo_env import ParallelPettingZooEnv
from pettingzoo_example import posg_env_v0
from ray.rllib.policy.policy import PolicySpec
from ray import air, tune
import gymnasium as gym
from ray.rllib.algorithms.algorithm_config import AlgorithmConfig
from ray.rllib.algorithms.ppo import PPOConfig


class CustomAgentPolicy(Policy):
    def compute_actions(
        self,
        obs_batch,
        state_batches=None,
        prev_action_batch=None,
        prev_reward_batch=None,
        info_batch=None,
        episodes=None,
        **kwargs,
    ):
        return [0 for x in obs_batch], state_batches, {}


def select_policy(agent_id, episode, **kwargs):
    return "custom_policy"


if __name__ == "__main__":
    # Import the pettingzoo environment in the way needed by rllib
    env_name = "posgenv_v0"
    register_env(
        env_name,
        lambda config: ParallelPettingZooEnv(
            posg_env_v0.PosgEnvironment(posg_env_v0.POSG())
        ),
    )
    config = (
        PPOConfig()
        .environment("posgenv_v0")
        .multi_agent(
            policies={
                "custom_policy": PolicySpec(policy_class=CustomAgentPolicy)
            },
            policy_mapping_fn=select_policy,
            policies_to_train=["custom_policy"],
        )
    )
    # ray.init()
    # algo = config.build()
    # algo.train()
    # algo.stop()
    # ray.shutdown()

    env = gym.make(env_name)
    obs = env.reset()
    done = False
    total_reward = 0.0
    while not done:
        action = {}
        for agent_id, agent_obs in obs.items():
            policy_id = config["multiagent"]["policy_mapping_fn"](
                agent_id, None, None
            )
            action[agent_id] = trainer.compute_single_action(
                agent_obs, policy_id=policy_id, unsquash_action=True
            )

        obs, reward, done, info = env.step(action)
        done = done["__all__"]

        total_reward += sum(reward.values())

    print(f"total-reward={total_reward}")
