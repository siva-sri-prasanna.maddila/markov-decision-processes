import functools

# import gymnasium
import numpy as np
from gymnasium.spaces import Discrete

# from pettingzoo import ParallelEnv
from copy import copy
from numpy import random

from pettingzoo.utils.env import ParallelEnv


class POSG:
    """POSG representation.

    :param list of int statespace: the states of the POSG.
    :param int nplayers: the number of players of the POSG.
    :param dict actionspaces: A dictionary player -> available actions.
    :param dict transition: A dictionary (state x joint action) -> tuple of
    probabilitites of successor state.
    :param dict rewards: A dict (state x joint action) -> tuple of rewards.
    """

    def __init__(
        self,
        statespace=None,
        nplayers=None,
        actionspaces=None,
        transition=None,
        rewards=None,
        obs_prob=None,
    ):
        if statespace is None:
            statespace = list(range(2))
        self.statespace = statespace
        if nplayers is None:
            nplayers = 2
        self.nplayers = nplayers
        if actionspaces is None:
            actionspaces = {0: range(2), 1: range(2)}
        self.actionspaces = actionspaces
        if transition is None:
            transition = dict()
            for s in range(2):
                for a0 in self.actionspaces[0]:
                    for a1 in self.actionspaces[1]:
                        p = (1 + 4 * s + 2 * a0 + a1) / 8
                        transition[(s, a0, a1)] = [p, 1 - p]
        self.transition = transition
        if rewards is None:
            rewards = dict()
            for s in range(2):
                for a0 in self.actionspaces[0]:
                    for a1 in self.actionspaces[1]:
                        r0 = 1 + 4 * s + 2 * a0 + a1
                        r1 = 1 + 4 * a1 + 2 * a0 + s
                        rewards[(s, a0, a1)] = [r0, r1]
        self.rewards = rewards

        # if P(Obs) not defined explicitly,
        # assume that it Obs = States i.e.
        # P(state, action, obs=state) = 1.0
        # source: https://www.st.ewi.tudelft.nl/mtjspaan/pub/Spaan12pomdp.pdf
        if obs_prob is None:
            # default to 2 states, 2 actions
            # and 2 observations for each agent
            obs_prob = []
            for i in range(self.nplayers):
                obs_prob.append(np.stack([np.eye(2) for _ in range(2)]))
        self.obs_prob = obs_prob

    def __str__(self):
        toprint = "POSG:\n"
        toprint += f"Number of players: {self.nplayers}\n"
        toprint += f"State space: ["
        for s in self.statespace:
            toprint += f"{s}, "
        toprint = toprint[:-2] + "]\n"
        toprint += "Action spaces:\n"
        for n in range(self.nplayers):
            toprint += f"   - Player {n}: ["
            for a in self.actionspaces[n]:
                toprint += f"{a}, "
            toprint = toprint[:-2] + "]\n"
        toprint += "Transitions: (s,a0,...,an-1) -> p over S\n"
        for tup in self.transition.keys():
            toprint += f"{tup} -> {self.transition[tup]}\n"
        toprint += "Rewards: (s,a0,...,an-1) -> [r0,...rn-1]\n"
        for tup in self.rewards.keys():
            toprint += f"{tup} -> {self.rewards[tup]}\n"
        return toprint


class PosgEnvironment(ParallelEnv):
    """A POSG environment, which subclasses the ParalllelEnv class."""

    def __init__(self, posg, discount=1, horizon=10):
        self.posg = posg
        self.s = len(posg.statespace)
        self.discount = discount
        self.horizon = horizon
        self.t = 0
        self.state = None
        self.observations = self.state
        self.possible_agents = [
            "player_" + str(r) for r in range(posg.nplayers)
        ]
        self.agents = copy(self.possible_agents)
        print(f"Agents' list: {self.agents}")
        self.agent_name_mapping = dict(
            zip(self.possible_agents, list(range(len(self.possible_agents))))
        )
        self.action_spaces = {
            self.agents[n]: Discrete(len(self.posg.actionspaces[n]))
            for n in range(self.num_agents)
        }
        print("self.action_spaces", self.action_spaces)
        print("type(self.action_spaces)", type(self.action_spaces))
        self.observation_spaces = {
            agent: Discrete(self.s) for agent in self.possible_agents
        }
        print("self.observation_spaces", self.observation_spaces)
        print("type(self.observation_spaces)", type(self.observation_spaces))

        # creating the observation probabilities for each agent
        self.posg.obs_prob = dict(zip(self.agents, self.posg.obs_prob))
        print(f"OBS_PROB: {self.posg.obs_prob}")

        self.infos = {agent: {} for agent in self.agents}
        self.rewards = None
        self.truncations = None
        self.terminations = None

    def reset(self, seed=None, return_info=False, options=None):
        self.agents = copy(self.possible_agents)
        self.state = Discrete(self.s).sample()
        print(f"Initial state: {self.state}")
        self.t = 0
        self.observations = {agent: self.state for agent in self.agents}

        if not return_info:
            return self.observations
        else:
            self.infos = {agent: {} for agent in self.agents}
            return self.observations, self.infos

    def observe(self, agent):
        """
        Returns the agent's current observations
        without calculating for the new state.
        """
        return self.observations[agent]
        # return np.array(self.observations[agent])

    def step(self, actions):
        """
        step(actions) takes in an action for each agent and should
        return the
        - observations
        - rewards
        - terminations
        - truncations
        - infos
        dicts where each dict is {agent_1: item_1, agent_2: item_2}
        """
        self.t += 1
        s = self.state
        tup = (s,)
        for agent in self.agents:
            tup = tup + (actions[agent],)
        prob = self.posg.transition[tup]
        next_s = np.random.choice(a=range(len(prob)), p=prob)
        self.state = next_s
        self.observations = {
            agent: self.get_obs(next_s, actions, agent)
            for agent in self.agents
        }
        self.rewards = {
            self.agents[n]: self.posg.rewards[tup][n]
            for n in range(self.num_agents)
        }
        self.infos = {agent: {} for agent in self.agents}
        self.terminations = {agent: False for agent in self.agents}
        self.truncations = {agent: False for agent in self.agents}
        if self.t >= self.horizon:
            self.agents = []
            for agent in self.truncations.keys():
                self.truncations[agent] = True
                self.terminations[agent] = True

        print(f"At time {self.t}:")
        print(f"Actions: {actions}")
        print(f"Observations: {self.observations}")
        print(f"Rewards: {self.rewards}")
        print(f"Terminations: {self.terminations}")
        print(f"Truncations: {self.truncations}")
        print(f"Remaining agents: {self.agents}")

        return (
            self.observations,
            self.rewards,
            self.terminations,
            self.truncations,
            self.infos,
        )

    def get_obs(self, state, actions, agent):
        """
        Returns an observation for a given state
        given the probability matrix for the POSG
        and the current actual state of the POSG.
        For an agent i,
            O_i(s, a, o) := P(Observing o in state s | agent=i, joint_actions=a)
        and we sample from the probability space
        using according to these probabilities.
        """
        # get observation probabilites for agent
        action = actions[agent]
        obs_local = self.posg.obs_prob[agent][state, :, action]

        # sample the observation space using obs_local
        return np.random.choice(range(self.s), p=obs_local)

    def state(self):
        return self.state

    def render(self):
        pass

    def close(self):
        pass

    @functools.lru_cache(maxsize=None)
    def action_space(self, agent):
        return self.action_spaces[agent]

    @functools.lru_cache(maxsize=None)
    def observation_space(self, agent):
        return self.observation_spaces[agent]


from pettingzoo.test import parallel_api_test  # noqa: E402

if __name__ == "__main__":
    example_posg = POSG()
    print(example_posg)
    # num_cycle should be lower than horizon, or there will be no agent left.
    # and an error will result.
    parallel_api_test(PosgEnvironment(example_posg), num_cycles=20)
    # Remarks: Trajectories of length "horizon" are always conducted till the
    # end.
    # In theory, there are ceil(num_cyles/horizon) such trajectories.
    # However, in parallel_test.py, the number of trajectories is upper-bounded
    # by 2.
