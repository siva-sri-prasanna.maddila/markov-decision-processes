# Partially-Observed Stochastic Game

A simple PettingZoo environment representing a POSG with two players, 
two actions each and three time steps (and fully-observable).

It passes the parallel environment test, which is a pettingzoo procedure 
(called in the main loop of the environment).