# Markov Decision Processes

This repository is an implementation of Markov Decision Processes in Gymnasium (used to be Gym) in Python. 


## MDP

The [mdp](./mdp/) module implements all the classes required to make a generic MDP with finite states and actions. The MDP environment is implemented as a Gymnasium environment in [mdp/mdp.py](mdp/mdp.py). The agents who choose the actions of the MDP, and also learn the optimal policy/strategy are implemented as concrete classes of `Agent`, an abstract class implemented in [mdp/agent.py](mdp/agent.py). For example, the agent who uses the Value Iteration method is implemented in the class `ValueIterationMethod` (see file: [valueIterationAgent.py](mdp/valueIterationAgent.py). Lastly, each problem of simulation/learning is implemented as an MDP-Agent tuple in the class `Problem` (see file: [Problem.py](mdp/problem.py) ).


# PettingZoo(Example)

This is a clone of the repository at [pettinzoo_example](https://forgemia.inra.fr/chip-gt/pettingzooexample), maintained here for modifications not intended for the current repository. Currently, the plan is to work with observable states and modify it as required.

## Virtual Environment 

The standard virtual environment can be setup using the `init.sh` file as follows: 

```bash
chmod +x init.sh
./init.sh
```

This creates a python virtual environment called `.venv`, and also installs the required packages as listed in [requirements.txt](./requirements.txt). If all goes well, the environment will test itself by running the Lunar Lander program in `mdp/`.

Activating and deactivating can be done normally as follows: 

```bash
# activation
source ./.venv/bin/activate

# deactivation
deactivate
```

