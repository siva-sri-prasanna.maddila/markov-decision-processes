"""
Module to implement the Q-Learning method for Markov Decision Processes.
It inherits from the abstract Agent class, which already provides the default
method to choose a new action.
"""

import numpy as np
from tqdm import tqdm
from gymnasium import spaces
from mdp import MDP
from agent import Agent


class QLearningAgent(Agent):
    """
    This class implements the Value Iteration method of
    learning the optimal policy for a given MDP.
    """

    def __init__(
        self,
        pi: list,
        num_states: int = 0,
        num_actions: int = 0,
        learning_rate: float = 0.9,
        discount_factor: float = 0.8,
        epsilon: float = 0.9,
        eps_decay_rate: float = 0.95,
        eps_final_val: float = 0.1,
    ):
        """
        Initialise the strategy used by the agent, as well
        as locations for the Values.
        """
        # learning parameters
        super().__init__(pi, discount_factor)
        self.alpha = learning_rate  # affects perceived reward
        self.eps = epsilon  # probability of exploration

        # stores agent policy and values
        self.value_func = np.zeros(num_states)
        self.q_func = np.zeros((num_states, num_actions))

        # action taken by the agent on the current time-step
        self.curr_action = None

        # updating epsilon after each epoch
        self.eps_decay_rate = eps_decay_rate
        self.eps_final_val = eps_final_val

    def __call__(self, curr_state: int, action_space: spaces.Discrete) -> int:
        """
        This method returns the choice of action for an agent based on the
        current state, available actions and also internal parameters. This
        overrides the default method used by the Agent class.
        """
        if np.random.rand() < self.eps:
            # explore, randomly choose an action.
            self.curr_action = super().__call__(curr_state, action_space)
        else:
            # exploit, choose the best action according to q-values
            self.curr_action = np.argmax(self.q_func[curr_state, :])

        return self.curr_action

    def update(self, state: int, s_prime: int, action: int) -> list[float]:
        """
        Function to implement one iteration of the Q-Learning
        method for a Markov Decision Process. Example implementation available at:
        https://gymnasium.farama.org/tutorials/training_agents/blackjack_tutorial/
        """
        # update q-values
        try:
            delta = (
                self.curr_reward
                + self.beta * np.max(self.q_func[s_prime, :])
                - self.q_func[state, action]
            )
            self.q_func[state, action] += self.alpha * delta
        except KeyError:
            # Passing over undefined states
            pass

        # Update the Value and Policy using q-values
        self.value_func[state] = np.max(self.q_func[state, :])

        s_new_policy = np.zeros(len(self.pi[state]))
        s_new_policy[np.argmax(self.q_func[state, :])] = 1.0
        self.pi[state] = s_new_policy

        # return calculated policy and values
        return self.value_func, self.pi

    def update_learning_rate(self):
        """
        Function to update the learning rate based
        """
        self.eps = max(self.eps_final_val, self.eps * self.eps_decay_rate)

    def learn(self, mdp: MDP) -> list[float]:
        """
        Simulates the method for self.num_episodes
        while updating the policy and values at the end.
        """
        for _ in tqdm(range(self.num_episodes), total=self.num_episodes):
            # Agent gets the current action
            curr_action = self(mdp.curr_state, mdp.action_space)

            # Simulate until the MDP is done.
            done = False
            while not done:
                # The MDP world evolves based on that action
                # and returns the reward for that (state,action)
                _, reward, done, _ = mdp.step(curr_action)

                # update the agent's perceived reward for taking
                # that action at that time step.
                # Note: (t - 1) because curr_time is already updated
                self.receive_reward(reward, mdp.curr_time - 1)

                # Now update policy and values
                self.update(mdp.old_state, mdp.curr_state, curr_action)

            # change learning rate after an episode
            self.update_learning_rate()

            # and reset for next episode
            mdp.reset()

        return self.value_func, self.pi
