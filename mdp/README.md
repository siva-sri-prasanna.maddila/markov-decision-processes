# MDP and Agents

### Main 
--- 

The [main.py](src/main.py) implements the following discounted reward MDP: 

| -        | State 1          | State 2         | State 3          |
|----------|------------------|-----------------|------------------|
| Action 1 | -5 \| (0.9,0,0.1) | 5 \| (0,1,0)     | 20 \| (0.9,0.1,0) |
| Action 2 | 10 \| (0,1,0)     | 0 \| (0.8,0.2,0) | undef            |


with a discount factor $\beta = 0.8$, which should have the optimal values $v_\beta = [28.9, 25, 42.8]$. Based on the choice of initial state and type of learning algorithm, performs a simulation and executes the learning algorithm. This can be called via the following: 

```bash
python main.py <initial-state> <agent-type = q or v>
```

Initial state can be an integer from 0 to 2, inclusive. Agent type is a string that chooses the agent: `q` uses a `QLearningAgent` and `v` uses a `ValueIterationAgent`.


**Note** that in the implementation, all states are zero-indexed i.e. State 1 from the MDP defined above is implemented as State 0 in the code.

### MDP
--- 

Each MDP is a concrete instance of the `MDP` class. This implements a generic (usually finite number of states and actions) MDP that allows mixed strategies. The MDP is essentially implemented as a tuple of States, Actions, Transition probabilities and Rewards. Furthermore, it is also provided with an internal notion of time to mark its evolution with each call to step.


### Agent
--- 

Each learning agent derives their implementation from the abstract class `Agent.py` (see file: [agent.py](./agent.py)). 

- An agent chooses an action depending on the current state and available actions. This is done using an internal strategy $\pi$. In the code. `Agent(state, action_space)` takes the current state and the possible actions to return the next action. 
- The agent receives a reward via the `receive_reward` method that calculates the perceived reward for this action. Note that the perceived reward can be different from the MDP reward. This is usually done via discounting and the parameter `beta` ($\beta$).
- `update` is an abstract method that performs one iteration of the learning algorithm. It performs one iteration, and the `Problem` itself carries out the learning until convergence. This has to be implemented for each agent.


### Problem
--- 

Lastly, an MDP and an Agent are packaged together into one instance of `Problem` (see file: [problem.py](./problem.py)). This allows an MDP to be aware of its Agent, and an Agent to be aware of thee MDP it is trying to simulate/learn.
    - A `Problem` exposes the `simulate` and the `calculate_optimal_agent_policy` methods.
    - `simulate` simulates one episode of the MDP and returns the cumulative reward for this episode.
    - `calculate_optimal_agent_policy` uses the `Agent.update` method to calculate the optimal values and policy for a fixed number of epochs. 

---
#### References 
- Filar, Jerzy, and Koos Vrieze. Competitive Markov decision processes. Springer Science & Business Media, 2012.
