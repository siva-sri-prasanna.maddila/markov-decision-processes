""" agent.py
This class implements an interface for MDP agents, 
including their strategy/policy variables `f`. Confusingly, 
this uses the Strategy design pattern to implement 
any agent, for example, ValueIterationAgent or QLearningAgent.
"""

from abc import ABC as AbstractBaseClass, abstractmethod
import numpy as np
from gymnasium import spaces


class Agent(AbstractBaseClass):
    """
    This is the abstract class that all agents
    derive from. It cannot be instantiated, since
    it does not make sense to have a base Agent
    without an implemented learning strategy.
    """

    def __init__(self, pi, discount_factor: float = 0.8):
        self.pi = pi
        self.beta = discount_factor
        self.curr_reward, self.total_reward = 0.0, 0.0

    def __call__(self, curr_state: int, action_space: spaces.Discrete) -> int:
        """
        This method implements the core logic of an agent.
        It samples an action from the action space according to
        probabilities defined by the strategy and current state.
        The default implementation is defined by default for all
        concrete classes implementing this class.
        """
        # using a mask that is 1 only for the action
        # chosen using the probas chosen using the strategy.
        action_probas = self.pi[curr_state]
        if len(action_probas) != action_space.n:
            # Undefined states : pad action_probas
            # on the right to the same length
            action_probas = np.pad(
                action_probas, (0, action_space.n - len(action_probas))
            )
        mask = np.zeros(action_space.n, dtype=np.int8)
        mask[np.random.choice(range(action_space.n), p=action_probas)] = 1
        return action_space.sample(mask=mask)

    def receive_reward(self, reward: float, timestep: int) -> float:
        """
        This method updates the agent's received rewards
        for having taken the action in their MDP. The perceived
        reward by the agent is discounted by self.beta, and is
        not necessarily the same as the actual reward of the MDP.
        """
        self.curr_reward = reward
        self.total_reward += self.curr_reward * (self.beta**timestep)
        return self.total_reward

    @abstractmethod
    def update(self, *args, **kwargs):
        """
        This function should implement one iteration of the
        agent's learning method. For example, ValueIterationAgent
        uses the VI algorithm and implements it here.
        """
        raise NotImplementedError

    @abstractmethod
    def learn(self, *args, **kwargs):
        """
        This function should implement the learning algorithm of the
        agent. This is different from update to allow for generic
        learning between agents.
        """
        raise NotImplementedError
