"""
This module implements the MDP class, a Gym environment
that models a Markovian Decision Process.
"""

import numpy as np
import gymnasium as gym
from gymnasium import spaces


class MDP(gym.Env):
    """
    Class to implement a stationary Markov Decision Process
    as a gym environment.
    """

    def __init__(
        self,
        num_actions: int,
        num_states: int,
        rewards: dict,  # r(s,a) ~> R
        transitions: dict,  # (s,a) ~> s'
        init_state: int = 0,  # initial state
        timesteps: int = 100,
    ):
        """
        Initialising the MDP with `num_states` states and
        `num_actions` actions using spaces.Discrete.
        Note: Both spaces are zero-indexed !!!
        """
        # counters for determining end of run
        self.timesteps, self.curr_time = timesteps, 0

        # main variables
        self.observation_space = spaces.Discrete(num_states)
        self.action_space = spaces.Discrete(num_actions)
        self.rewards = rewards
        self.transitions = transitions
        self.curr_reward = 0.0

        if self.rewards.keys() != self.transitions.keys():
            raise RuntimeError(
                "Rewards and Transitions defined on different domains"
            )
        if init_state < 0 or init_state >= num_states:
            raise RuntimeError(
                f"Invalid init state{init_state}: out of bounds!"
            )

        # storing state-related variables
        self.init_state = init_state  # storing this for reset()
        self.curr_state = init_state
        self.old_state = init_state

    def reset(self, seed=None, options=None):
        """
        Reset the MDP to run the next trajectory
        """
        super().reset(seed=seed)
        self.curr_state = self.init_state
        self.old_state = None
        self.curr_time = 0

    def step(self, action: int):
        """
        Simulate a step in an MDP. This means an action is taken,
        so that s ---> s'. It returns the reward for this transition.
        Note that the agent keeps track of their total rewards.
        """
        try:
            self.transitions[(self.curr_state, action)]
        except KeyError:
            # Using info parameter to return status
            return (
                self.curr_state,
                0.0,
                self.curr_time == self.timesteps,
                "Invalid state accessed. Not stepping.",
            )
        # state with defined transitions, stepping.
        probas = self.transitions[(self.curr_state, action)]
        self.curr_reward = self._get_reward(action)
        self.old_state = self.curr_state
        self.curr_state = self._get_state(
            probas
        )  # old_state <- curr_state, curr_state <- new_state
        self.curr_time += 1
        # returning results of step
        return (
            self.curr_state,
            self.curr_reward,
            self.curr_time == self.timesteps,
            None,
        )

    def _get_state(self, probas):
        """
        Helper function to return the current state,
        using the probability distribution over each
        state in self.observation_space
        """
        # Create a mask that is 1 only where we select the element.
        # this mask has to be generated using transition probability
        # as defined in `probas`.
        mask = np.zeros(self.observation_space.n, dtype=np.int8)
        mask[np.random.choice(range(self.observation_space.n), p=probas)] = 1

        # Saving current state and selecting new state using observation space
        self.old_state = self.curr_state
        return self.observation_space.sample(mask=mask)

    def _get_reward(self, action):
        """
        Helper function to calculate the reward for each
        timestep, using the old state and the action.
        """
        try:
            return self.rewards[(self.curr_state, action)]
        except KeyError:
            # When accessing undefined states.
            return 0.0


# def _calculate_P(self):
#    """
#    Helper function to calculate the transition matrix P
#    as defined by the Markov Decision Process. This relies on
#    the object being well-initialised first.
#    """
#    raise RuntimeError("_calculate_P should not be explicit.")
#    self.P = np.zeros((self.observation_space.n, self.observation_space.n))
#    for i in range(self.observation_space.n):
#            local_f = self.f[i]
#            for j in range(self.observation_space.n):
#                for k in range(len(local_f)):
#                    # this length is similar to number
#                    # of actions for this state.
#                    self.P[i, j] += local_f[k] * self.transitions[(i, k)][j]
