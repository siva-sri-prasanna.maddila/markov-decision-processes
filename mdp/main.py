"""
This module is the main test code. It creates an MDP, a ValueIterations agent
and packages them into a Problem. Then, it simulates their interactions, and 
finally calculates the optimal policy for the agent, as a function of its MDP.
"""

import sys
import multiprocessing as mp
from functools import partial
from tqdm import tqdm
from agent import Agent  # for type information
from value_iteration_agent import ValueIterationAgent
from q_learning_agent import QLearningAgent
from mdp import MDP


def create_prob(init_state: int = 0, agent_type: str = "q"):
    """
    Creates a reference problem to help test the classes
    """
    # simulating the text example.
    mdp_states, mdp_actions = 3, 2

    # rewards are represented as (curr_state,action)->reward key-value pairs
    mdp_rewards = {(0, 0): -5, (0, 1): 10, (1, 0): 5, (1, 1): 0, (2, 0): 20}

    # transition probabilities are represented as a dict of
    # (curr_state, action)-> proba_of_state[idx] key-value pairs
    mdp_transitions = {
        (0, 0): [0.9, 0, 0.1],
        (0, 1): [0, 1, 0],
        (1, 0): [0, 1, 0],
        (1, 1): (0.8, 0.2, 0),
        (2, 0): [0.9, 0.1, 0],
    }

    # Strategy used by the MDP
    # strat[state] = list[proba_of_choosing_action: float]
    # Note: here, state 3 only defines action 1, so it is
    #       a singleton.
    agent_strategy = [(0.1, 0.9), (1.0, 0.0), (1.0,)]

    # just checking
    assert mdp_transitions.keys() == mdp_rewards.keys()

    mdp = MDP(
        mdp_actions,
        mdp_states,
        mdp_rewards,
        mdp_transitions,
        init_state=init_state,
        timesteps=1000,
    )

    # define the agent
    if agent_type == "v":
        agent = ValueIterationAgent(
            pi=agent_strategy, num_states=mdp_states, discount_factor=0.8
        )
    elif agent_type == "q":
        # define the qLearning agent
        agent = QLearningAgent(
            pi=agent_strategy, num_states=mdp_states, num_actions=mdp_actions
        )
    else:
        raise RuntimeError(f"Agent type {agent_type} not found")
    return mdp, agent


def simulate(mdp: MDP, agent: Agent):
    """
    Simulates an agent running an MDP until done.
    """
    done = False
    while not done:
        # agent chooses an action
        curr_action = agent(mdp.curr_state, mdp.action_space)

        # The MDP world evolves based on that action
        # and returns the reward for that (state,action)
        _, reward, done, _ = mdp.step(curr_action)

        # update the agent's perceived reward for taking
        # that action at that time step.
        # Note: (t - 1) because curr_time is already updated
        agent.receive_reward(reward, mdp.curr_time - 1)

    mdp.reset()
    return agent.total_reward


def create_and_simulate_prob(init_state: int = 0, agent_type: str = "q"):
    """
    Function to create an MDP and run an epoch, to get the average reward.
    Parameters are hardcoded for now.
    """
    mdp, agent = create_prob(init_state, agent_type)

    # simulate and return the reward
    return simulate(mdp, agent)


if __name__ == "__main__":
    # choose the initial state using a partial function
    INIT_STATE = int(sys.argv[1]) if len(sys.argv) > 1 else 0

    # Choose the agent to test
    AGENT_TYPE = sys.argv[2] if len(sys.argv) > 2 else "q"

    # agent type
    print(
        f"Setting initial state to  {INIT_STATE} and agent type to {AGENT_TYPE}"
    )

    # define the parameters of the run
    num_episodes = int(sys.argv[3]) if len(sys.argv) > 3 else 100
    record_list = []

    # running it in parallel for speed
    worker = partial(create_and_simulate_prob, agent_type=AGENT_TYPE)
    with mp.Pool() as pool:
        for result in tqdm(
            pool.imap_unordered(
                worker, [INIT_STATE for _ in range(num_episodes)]
            ),
            total=num_episodes,
        ):
            record_list.append(result)

    # checking that we did not lose any results
    assert len(record_list) == num_episodes

    # results
    print(
        f"Average reward over {num_episodes} runs = {sum(record_list) / len(record_list)}"
    )

    # calculating the optimal policy
    mdp, agent = create_prob(init_state=INIT_STATE, agent_type=AGENT_TYPE)
    if type(agent) == QLearningAgent:
        # For the qLearningAgent, we have to
        # set the number of epochs
        agent.num_episodes = num_episodes

    print(f"Optimal values and policy are : {agent.learn(mdp)}")
