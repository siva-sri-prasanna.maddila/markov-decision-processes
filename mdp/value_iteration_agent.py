"""
Module to implement the value iterations method for Markov Decision Processes.
This will determine the right strategy values.
"""

import numpy as np
from mdp import MDP
from agent import Agent


class ValueIterationAgent(Agent):
    """
    This class implements the Value Iteration method of
    learning the optimal policy for a given MDP.
    """

    def __init__(
        self, pi: list, num_states: int = 0, discount_factor: float = 0.8
    ):
        """
        Initialise the strategy used by the agent, as well
        as locations for the Values.
        """
        super().__init__(pi, discount_factor)
        self.value_func = np.zeros(num_states)

    def update(self, mdp: MDP) -> list[float]:
        """
        Function to implement one iteration of the value iteration
        method for a Markov Decision Process. Pseudo-code available at:
        https://www.cs.cmu.edu/afs/cs/project/jair/pub/volume4/kaelbling96a-html/node19.html
        """
        q_func = np.zeros((mdp.observation_space.n, mdp.action_space.n))
        delta = 0.0
        v_next = np.copy(self.value_func)
        for state in range(mdp.observation_space.n):
            # update q-values
            for action in range(mdp.action_space.n):
                try:
                    q_func[state, action] = mdp.rewards[
                        state, action
                    ] + self.beta * sum(
                        (
                            mdp.transitions[(state, action)][s_prime]
                            * self.value_func[s_prime]
                            for s_prime in range(self.value_func.shape[0])
                        )
                    )
                except KeyError:
                    # Passing over undefined states
                    continue

            # Update the Value and Policy using q-values
            v_next[state] = np.max(q_func[state, :])

            s_new_policy = np.zeros(len(self.pi[state]))
            s_new_policy[np.argmax(q_func[state, :])] = 1.0
            self.pi[state] = s_new_policy

            delta = max(delta, abs(self.value_func[state] - v_next[state]))

        # return calculated policy and values
        self.value_func = v_next
        return self.value_func, self.pi, delta

    def learn(self, mdp: MDP, eps: float = 1e-5) -> list[float]:
        """
        Implements the learning algorithm.
        """
        delta = 2 * eps
        while delta > eps:
            # update until convergence
            values, pi, delta = self.update(mdp)

        # return the optimal values
        return values, pi
