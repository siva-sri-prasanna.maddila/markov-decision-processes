"""
Installation test file for PettingZoo: AEC Environment.
Source: https://pettingzoo.farama.org/api/aec/
This is (not yet) meant for serious development.
"""

from pettingzoo.classic import chess_v5

env = chess_v5.env(render_mode="human")

env.reset()
for agent in env.agent_iter():
    observation, reward, termination, truncation, info = env.last()
    if termination or truncation:
        action = None
    else:
        action = env.action_space(agent).sample(
            observation["action_mask"]
        )  # this is where you would insert your policy
    env.step(action)
env.close()
