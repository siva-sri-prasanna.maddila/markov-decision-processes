"""
Installation test file for PettingZoo: Parallel Environment.
Source: https://pettingzoo.farama.org/api/parallel/
This is (not yet) meant for serious development.
"""

from pettingzoo.butterfly import pistonball_v6

parallel_env = pistonball_v6.parallel_env(render_mode="human")
observations = parallel_env.reset()

while parallel_env.agents:
    actions = {
        agent: parallel_env.action_space(agent).sample()
        for agent in parallel_env.agents
    }  # this is where you would insert your policy
    (
        observations,
        rewards,
        terminations,
        truncations,
        infos,
    ) = parallel_env.step(actions)
    parallel_env.render()

parallel_env.close()
